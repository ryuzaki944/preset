import React, { useEffect, useState } from 'react'
import { Provider } from 'react-redux';
import store from './store/store';
import 'antd/dist/antd.css';
import './index.css';
import {getData} from './store/actions/getDataAction';
//Components
import Main from './components/main';

function App() {
  return (
    <Provider store={store}>
      <div style={{ margin: 50}}>
        <Main />
      </div>
    </Provider>
  );
}

export default App;
