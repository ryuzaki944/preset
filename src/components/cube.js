import React, { useEffect, useState } from 'react'
import AlertShow from './alertShow'

function Cube(props) {

    const [color, setColor] = useState('white')

    const [color2, setColor2] = useState('blue')

    const [history, setHistory] = useState([])

    const {mode} = props.data

    // reset Cells
    useEffect(() => {
        let doc1 = document.getElementsByClassName('cell')
        if(doc1.length > 0){
            for (let i in doc1){
                if(doc1[i].style){
                    doc1[i].style.backgroundColor = 'white'
                }
            }
        }
        setHistory([])
    },[mode])


    let numData = []

    for (let i = 0; i < mode; i++) {
        for (let j = 0; j < mode; j++) {
            numData.push({
                i: i,
                j: j,
            })
        }
    }

    let cellHover = (e) => {
        if (e.target.style.background === color){
            e.target.style.background = color2
        } else {
            e.target.style.background = color
        }

        let getAttributes = e.target.attributes
        let j = getAttributes.dataX.value
        let i = getAttributes.dataY.value

        const response = [{i, j}, ...history]

        if(response.length > 15){
            response.splice(10, 1)
        }        
        setHistory(response)  
    }

    //Component
    let Square = (i, j) => <div className="cell" onMouseEnter={cellHover}  style={{backgroundColor: color}} dataY={i} dataX={j} ></div>
    
    let cubeData = []

    cubeData = numData?.map(item => (
        Square(item.i, item.j)
    ))        

    return (
        <div style={{display: 'flex', position: 'relative'}}>
            <div id="map" style={{ width: `${mode * 50 + 10}px`}}>
                {cubeData}
            </div> 
            <div>
                <AlertShow data={history} />
            </div> 
        </div>
    )
}

export default Cube;