import React, { useEffect, useState } from 'react'
import {Alert} from 'antd'

function AlertShow(props) {

    const history = props.data
    
    let Informator = (i, j) => <Alert message={"row " + i + " col " + j} type="warning" style={{width: 150, color: 'red', margin: '5px 0'}}><p style={{color: 'red'}}>  </p></Alert>

    let alertData = []

    //Proverka na Empty or not podobie if not v Python
    if(history && history.length > 0 ){
        alertData = history?.map(item => (
            Informator(item.i, item.j)
        ))
        console.log(Informator())
    }

    return (
        <div style={{ marginLeft: '50px', position: 'absolute', top: '-55px', width: 150}}>
            <h2 style={{fontWeight: 'bold'}}>Hover squares</h2>
            {alertData}
        </div>
    )
}

export default AlertShow;