import React, { useEffect, useState } from 'react'
import {getData} from '../store/actions/getDataAction'
import { connect } from 'react-redux';
import { Button, Select, Form } from 'antd';
import Cube from './cube'

function Main(props) {
        
    const [mode, setMode] = useState(0)

    useEffect(() => {
        props.getData()
    }, [])

    const {data} = props.getDataReducer

    // Obrashenie v data {easymode,normalmode, hardmode}
    const {easyMode, normalMode, hardMode} = data
    
    const allModes = [easyMode, normalMode, hardMode]
    
    let renderModes = []

    const { Option } = Select;

    // typeof arr[0] !== 'undefined' проверка для на элементы , undefined или нет
    if (typeof allModes[0] !== 'undefined'){
        renderModes = allModes?.map(item => (
            <Option value={item.field}>{item.field}</Option>
        ))
    } 
    
    let onChange = (value) => {
        // if (value !== mode.mode) console.log('hi')
    }
    
    let handleOk = (value) => {
        setMode(value)
    }
    
    return (
        <div style={{display: 'flex'}}>
            <Form onFinish={handleOk} style={{ display: 'flex', flexWrap: 'wrap', position: 'relative', width: `${mode.mode * 50 + 50}px`}} >
                <Form.Item name="mode">
                    <Select name="picker" placeholder="Pick Mode" style={{ width: 190 }} onChange={onChange}>
                        {renderModes}
                    </Select>
                </Form.Item>
                <Button type="primary" key="submit" htmlType='submit' style= {{ marginLeft: 10, width: 100 }}>START</Button>
                <Cube data={mode} />

            </Form>
            
        </div>
    )

}

const mapStateToProps=(state)=>({
    getDataReducer: state.getDataReducer,
})

export default connect(mapStateToProps, {getData})(Main);
