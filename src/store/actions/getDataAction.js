import {GET_DATA} from './types';
import axios from 'axios/index'

export const getData = () => (dispatch) => {    
    axios.get(`https://demo1030918.mockable.io/`)
    .then(response => {
        return dispatch ({
            type: GET_DATA,
            payload: response.data
        })
    })
}
